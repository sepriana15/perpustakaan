<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class book extends Model
{
    //
    protected $table = 'buku';
    protected $fillable = ['nama_buku', 'id_buku', 'penerbit', 'kategori_id'];

    public function kategori(){
        return $this -> hasMany(kategori::class);
    }
}
