<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    //
    protected $table = 'kategori';

    protected $fillable = ['nama', 'id'];

    public function book(){
        return $this->belongsTo(pustaka::class)->withPivot9(['kategori_id']);
    }


    public function kategori(){
        return $this ->belongsTo(kategori::class);
    }
}
