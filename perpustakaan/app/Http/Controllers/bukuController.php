<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class bukuController extends Controller
{
    public function index(){
        //mengambil data dari tabel buku
        //$databuku = DB::table('databuku')->get();
        $databuku = DB::table('databuku')->paginate(5);

        //mengirim data buku ke view index
        return view('index',['buku' => $databuku]);
    }

    //menampilkan view form
    public function tambah(){

        //memanggil view tambah
        return view('tambah');
    }

    public function store(Request $request){
        
        
        $request->validate([
        'nama_buku' => 'required',
        'penerbit_buku' => 'required'
        ]);
        
        DB::table('databuku')->insert([
            'nama_buku' => $request->nama_buku,
            'penerbit_buku'=> $request->penerbit_buku,
            'tgl_pinjam' => $request->tgl_pinjam,
            'tgl_kembali' => $request->tgl_kembali
        ]);

        return redirect('/buku');
    }

    //untuk edit data buku
    public function edit($id){
        $databuku = DB::table('databuku')->where('id_buku', $id)->get();
        
        return view('edit',['buku' => $databuku]);
    }

    //update data buku
    public function update(Request $request){
        DB::table('databuku')->where('id_buku', $request->id)->update([
            'nama_buku' => $request->nama_buku,
            'penerbit_buku' => $request->penerbit_buku,
            'tgl_pinjam' => $request->tgl_pinjam,
            'tgl_kembali' => $request->tgl_kembali
        ]);

        return redirect('/buku');
    }

    //hapus data buku
    public function hapus($id){
        DB::table('databuku')-> where ('id_buku', $id)->delete();

        return redirect('/buku');
    }

    public function input(){
        return view('index');
    }

    public function proses(Request $request){
        $this->validate($request,[
            'nama_buku'=> 'required',
            'id_buku'=> 'required\numeric',
            'penerbit_buku'=> 'required',
            'kategori'=> 'required'
            // 'tanggal_pinjam'=> 'required|numeric',
            // 'tanggal_kembali'=> 'required|numeric'
        ]);

        return view('index',['buku' => $request]);
    }
}
