<!DOCTYPE html>
    <html>
        <head>
            <title>Perpustakaan Online Sepriana </title>
        </head>

    <body>

        <style type="text/css">
            .pagination li{
                float: left;
                list-style-type: none;
                margin: 5px;
            }
        </style>


        <h2>Sepriana Carolina Library</h2>
        <h3>Data Buku</h3>

        <a href="/buku/tambah"> + Tambah Buku Peminjaman</a>
        <a href="/pustaka"> Pustaka Buku </a>

        <br/>
        <br/>

        <table border="1">
            <tr>
                
                <th>Nama Buku</th>
                <th>Penerbit Buku</th>
                <th>Kategori</th>
                <th>Opsi</th>
            </tr>

            @foreach($buku as $p)

            <tr>
                
                <td>{{$p->nama_buku}}</td>
                <td>{{$p->penerbit_buku}}</td>
                <td>{{$p->kategori}}</td>
            
                <td>
                    <a href="/buku/edit/{{$p->id_buku}}">Edit</a>
                    |
                    <a onclick="return confirm('Are you sure to delete this?')" href="/buku/hapus/{{$p->id_buku}}">Hapus</a>
                </td>
            </tr>

            @endforeach
        </table>

    <br/>
    <!-- Halaman : {{ $buku -> currentPage() }} <br/>
    Jumlah Data : {{ $buku -> total() }} <br/>
    Data Per Halaman : {{ $buku -> perPage() }} <br/> -->

    {{$buku -> links() }}


    </body>
</html>
