<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Buku Perpustakaan Sepriana</title>
 
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
</head>
<body>
 
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="card mt-5">
                        <div class="card-body">
                            <h3 class="text-center">Input Buku</h3>
                            <br/>
 
                            {{-- menampilkan error validasi --}}
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
 
                            <br/>
                             <!-- form validasi -->
                            <form action="/proses" method="post">
                                {{ csrf_field() }}
 
                                <div class="form-group">
                                    <label for="nama">Nama Buku</label>
                                    <input class="form-control class @error('nama') is-invalid @enderror" type="text" name="nama_buku" value="{{ old('nama_buku') }}">
                                </div>
                                <div class="form-group">
                                    <label for="pekerjaan">Id Buku</label>
                                    <input class="form-control" type="text" name="id_buku" value="{{ old('id_buku') }}">
                                </div>
                                <div class="form-group">
                                    <label for="usia">Penerbit</label>
                                    <input class="form-control" type="text" name="penerbit_buku" value="{{ old('penerbit') }}">
                                </div>
                                <div class="form-group">
                                    <label for="usia">Kategori</label>
                                    <input class="form-control" type="text" name="kategori" value="{{ old('kategori') }}">
                                </div>
                                <!-- <div class="form-group">
                                    <label for="usia">Tanggal Pinjam</label>
                                    <input class="form-control" type="text" name="tgl_pinjam" value="{{ old('tgl_pinjam') }}">
                                </div>
                                <div class="form-group">
                                    <label for="usia">Tanggal Kembali</label>
                                    <input class="form-control" type="text" name="tgl_kembali" value="{{ old('tgl_kembali') }}">
                                </div> -->
                                <div class="form-group">
                                    <input class="btn btn-primary" type="submit" value="Proses">
                                </div>
                            </form>
 
                        </div>
                    </div>
                </div>
            </div>
        </div>
   
</body>
</html>