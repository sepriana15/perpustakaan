<!DOCTYPE html>
    <html>
        <head>
            <title>Perpustakaan Online Sepriana </title>
        </head>

    <body>

        <h2>Sepriana Carolina Library</h2>
        <h3>Edit Data Peminjaman Buku</h3>

        <a href="/buku">Kembali</a>

        <br/>
        <br/>

        @foreach($buku as $p)
        <form action="/buku/update" method="post">
            {{ csrf_field()}}
            <input type="hidden" name="id" value="{{$p->id_buku}}"><br/>
            Nama buku <input type="text" required="required" name="nama_buku" value="{{$p->nama_buku}}"><br/>
            Penerbit Buku <input type="text" required="required" name="penerbit_buku" value="{{$p->penerbit_buku}}"><br/>
            Tanggal Pinjam <input type="date" required="required" name="tgl_pinjam" value="{{$p->tgl_pinjam}}"><br/>
            Tanggal Kembali <input type="date" required="required" name="tgl_kembali" value="{{$p->tgl_kembali}}"><br/>
            <input type="submit" value="Simpan Data">
        </form>
        @endforeach

        
    </body>
</html>
