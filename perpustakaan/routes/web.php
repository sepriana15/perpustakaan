<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('halo', function(){
    return "Selamat Datang pada Web Saya Sepriana Carolina Panggabean";
});

Route::get('blog', function(){
    return view('blog');
});


Route::get('buku', 'bukuController@index');

Route::get('/buku/tambah','bukuController@tambah');




Route::post('/buku/store', 'bukuController@store');

Route::get('/buku/edit/{id}','bukuController@edit');

Route::post('/buku/update', 'bukuController@update');

Route::get('/buku/hapus/{id}', 'bukuController@hapus');

Route::get('/pustaka', 'pustakaController@index');
